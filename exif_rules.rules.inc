<?php

/**
 * @file
 * Exif Rules: actions, conditions and events for the Rules module.
 */

/**
 * Implements hook_rules_action_info().
 */
function exif_rules_rules_action_info() {
  $actions = array(
    'exif_rules_remove_file_exif' => array(
      'label' => t('Remove Exif Metadata from File.'),
      'group' => t('Exif Rules'),
      'parameter' => array(
        'file' => array(
          'type' => 'file',
          'label' => t("File who's metadata will be erased"),
          'description' => t("Please specify the file who's Exif metadata you will erase."),
        ),
        'exif_tool' => array(
          'type' => 'text',
          'label' => t('Please pick exif tool you wish to use'),
          'options list' => 'exif_rules_get_tools_options',
          'description' => t('There is a variaty of tools defined in the system, feel free to pick one.'),
        ),
      ),
    ),
    'exif_rules_remove_entity_files_exif' => array(
      'label' => t('Remove Exif Metadata from all files of an Entity.'),
      'group' => t('Exif Rules'),
      'parameter' => array(
        'entity' => array(
          'type' => 'entity',
          'label' => t('Entity to get all the file-fields from'),
          'description' => t('Please specify the entity where from we should get all the file-fields.'),
        ),
        'exif_tool' => array(
          'type' => 'text',
          'label' => t('Please pick exif tool you wish to use'),
          'description' => t('Please separate formats by spaces, i.e.: png gif jpg jpeg'),
          'options list' => 'exif_rules_get_tools_options',
          'description' => t('There is a variaty of tools defined in the system, feel free to pick one.'),
        ),
      ),
    ),
  );

  return $actions;
}

/**
 * Implements hook_rules_condition_info().
 */
function exif_rules_rules_condition_info() {
  $conditions = array();

  $conditions['exif_rules_check_file_format'] = array(
    'label' => t('File has a correct format.'),
    'group' => t('Exif Rules'),
    'parameter' => array(
      'file' => array(
        'type' => 'file',
        'label' => t('File to check'),
      ),
      'extensions_filter' => array(
        'type' => 'text',
        'label' => t('Extensions Filter'),
        'restriction' => 'input',
      ),
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_event_info().
 */
function exif_rules_rules_event_info() {
  $events = array();

  $events['exif_rules_file_presave'] = array(
    'label' => t('File is about to be saved'),
    'group' => t('Exif Rules'),
    'variables' => array(
      'file' => array(
        'type' => 'file',
        'label' => t('File about to be saved'),
      ),
    ),
  );

  return $events;
}

/**
 * Callback function erases file's exif metadata.
 */
function exif_rules_remove_file_exif($file, $exif_tool) {
  $exif_tools = module_invoke_all('exif_rules_tools_info');
  $exif_tool_info = $exif_tools[$exif_tool];
  $function = $exif_tool_info['process'];

  $function(drupal_realpath($file->uri));

  return array('file' => $file);
}

/**
 * Callback function.
 * 
 * That erases exif metadata from all the file fields of an entity.
 */
function exif_rules_remove_entity_files_exif($entity, $exif_tool) {
  $fields = field_info_instances($entity->type(), $entity->getBundle());

  $spotted_fields = array();
  $supported_modules = array('file', 'image');
  // Now that we have all the fields,
  // we want to filter only those which were use file or image modules.
  foreach ($fields as $name => $field_values) {
    if (isset($field_values['widget']['module']) && in_array($field_values['widget']['module'], $supported_modules)) {
      $file_field_val = $entity->{$name}->value();
      if (!empty($file_field_val) && isset($file_field_val['fid'])) {
        $file = file_load($file_field_val['fid']);
        exif_rules_remove_file_exif($file, $exif_tool);
      }
      // There are cases with multi-value fields (delta > 1).
      elseif (!empty($file_field_val) && is_array($file_field_val)) {
        foreach ($file_field_val as $index => $value) {
          $file = file_load($value['fid']);
          exif_rules_remove_file_exif($file, $exif_tool);
        }
      }
    }
  }
}

/**
 * Callback function that checks file format.
 */
function exif_rules_check_file_format($file, $extensions) {
  if (file_exists($file->uri) && strlen($extensions) >= 3) {
    $path_parts = pathinfo($file->uri);
    $allowed_ext = explode(' ', $extensions);
    if (in_array(strtolower($path_parts['extension']), $allowed_ext)) {
      return TRUE;
    }
  }

  return FALSE;
}
